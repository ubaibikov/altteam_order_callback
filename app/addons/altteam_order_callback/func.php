<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/

use Tygh\Tygh;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_order_callback_place_order($request_id)
{
    $call_data = fn_get_call_data($request_id);
    $order_callback = unserialize($call_data['order_callback']);

    $product_data[$call_data['product_id']] = array(
        'product_id' => $call_data['product_id'],
        'amount' => $order_callback['amount'] ?? 1,
        'product_options' => !empty($order_callback['product_options']) ? $order_callback['product_options'] : ''
    );

    if (!empty($call_data['phone'])) {
        $auth  = Tygh::$app['session']['auth'];
        $order_id = fn_order_callback_cart_data($call_data, $product_data, Tygh::$app['session']['cart'], $auth);

        if ($order_id != 0) {
            $result['notice'] = __('order_callback.order_placed');
            fn_delete_call_request($request_id);
        }

        return $result;
    } else {
        $result['error'] = __('order_callback.order_dontplaced');
    }

    return $result;
}

function fn_order_callback_cart_data($params, $product_data, &$cart, &$auth)
{
    // Save cart
    $buffer_cart = $cart;
    $buffer_auth = $auth;

    $cart = array(
        'products' => array(),
        'recalculate' => false,
        'payment_id' => 0,
        'is_call_request' => true,
    );

    $firstname = $params['name'] ?? '';
    $lastname = '';

    $cart['user_data']['email'] = '';

    if (!empty($firstname) && strpos($firstname, ' ')) {
        list($firstname, $lastname) = explode(' ', $firstname);
    }

    $cart['user_data']['firstname'] = $firstname;
    $cart['user_data']['b_firstname'] = $firstname;
    $cart['user_data']['s_firstname'] = $firstname;
    $cart['user_data']['lastname'] = $lastname;
    $cart['user_data']['b_lastname'] = $lastname;
    $cart['user_data']['s_lastname'] = $lastname;
    $cart['user_data']['phone'] = $params['phone'];
    $cart['user_data']['b_phone'] = $params['phone'];
    $cart['user_data']['s_phone'] = $params['phone'];
    

    $auth['user_id'] = 0;

    foreach (array('b_address', 's_address', 'b_city', 's_city', 'b_country', 's_country', 'b_state', 's_state') as $key) {
        if (!isset($cart['user_data'][$key])) {
            $cart['user_data'][$key] = ' ';
        }
    }

    fn_add_product_to_cart($product_data, $cart, $auth);

    fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);

    Registry::set('runtime.company_id', $params['company_id']);

    $order_id = 0;

    if ($res = fn_place_order($cart, $auth)) {
        list($order_id) = $res;
    }

    $cart = $buffer_cart;
    $auth = $buffer_auth;

    Registry::set('runtime.company_id', 0);

    return $order_id;
}

function fn_get_call_data($request_id)
{
    return db_get_row("SELECT product_id, phone,name,company_id FROM ?:call_requests WHERE request_id = ?i", $request_id);
}

function fn_altteam_order_callback_do_call_request(&$params, &$product_data, $cart, &$auth, $company_id)
{
    if (empty($params['email']) && !empty($params['phone'])) {

        $serialize_callback_data  = array(
            'amount' => $product_data[$params['product_id']]['amount'],
            'product_options' => $product_data[$params['product_id']]['product_options'] ?? '',
        );

        $params['order_callback'] = serialize($serialize_callback_data);
    }
}