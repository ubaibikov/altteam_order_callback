<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/
use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'request') {
        if (!empty($_REQUEST['request_id'])) {
            $result = fn_order_callback_place_order($_REQUEST['request_id']);
             if (!empty($result['notice'])) {
                fn_set_notification('N', __('error'), $result['notice']);
                Tygh::$app['ajax']->assign('force_redirection', fn_url('call_requests.manage'));
            } else {
                fn_set_notification('E', __('error'), $result['error']);
            }
        }
    }
}